# simulate.py
# Miles Lucas - mdlucas@nrao.edu

import argparse
import os
from pathlib2 import Path

# CASA imports
from casac import casac
me = casac.measures()
sm = casac.simulator()
cl = casac.componentlist()
qa = casac.quanta()
ia = casac.image()
log = casac.logsink()
from tclean import tclean
from simutil import simutil

center = ['J2000','322d07m45.0', '+45d00m00.0']
Frequency = '5GHz'
abstime = '2018/06/01 12:00:00'
noise = '0.001Jy'
models = {
	'points':'sim/models/points.im',
}

def simulate(name, model='points', recreate_models=False):
	'''
	Creates synthesis image and single dish image from the name and model

	Params
	------
		name: str
			The basename for all the files, stored in the `sim` directory
		model: str {'points'} (optional)
			The model to base the simulations off of. Will raise a value error if 
			not a recognized model. Default='points'
		recreate_models: bool (optional)
			Force create all the model images. Note, the images will be created if they
			do not exist, so this will force recreate the models (useful if change to 
			model code). Default=False
	'''
	if not model in list(models):
		raise ValueError('Not a recognized model')

	# Create sim directory if it doesn't already exist
	os.system('mkdir -p sim')
	os.system('mkdir -p sim/models')

	if recreate_models:
		create_points_model()
	elif not Path(models['points']).is_dir():
		create_points_model()
	

	# Create interferometer image
	create_ms(name, model)
	clean(name)

	# Create single dish image
	sd_convolve(name, model)

def create_points_model():
	'''
	This creates an image based on four point sources with varying fluxes at one frequency.
	The position of the sources should be roughly quadrilateral about 20" or more apart.
	'''
	ia.fromshape(models['points'], [2400, 2400, 1, 1], overwrite=True)
	cs = ia.coordsys()
	cs.setunits(['rad', 'rad', '', 'Hz'])
	cell_rad = qa.convert(qa.quantity('0.1arcsec'), 'rad')['value']
	cs.setincrement([-cell_rad, cell_rad], 'direction')
	cs.setreferencevalue([qa.convert(center[1],'rad')['value'],
			qa.convert(center[2],'rad')['value']], type='direction')
	cs.setreferencevalue(Frequency, 'spectral')
	ia.setcoordsys(cs.torecord())
	ia.setbrightnessunit('Jy/pixel')
	cl.addcomponent(dir='J2000 322d07m45.0 +45d00m00.0', flux=0.2, freq=Frequency, shape='point')
	cl.addcomponent(dir='J2000 322d07m45.0 +44d59m20.0', flux=0.3, freq=Frequency, shape='point')
	cl.addcomponent(dir='J2000 322d07m15.0 +45d00m40.0', flux=0.1, freq=Frequency, shape='point')
	cl.addcomponent(dir='J2000 322d08m25.0 +45d00m00.0', flux=0.25, freq=Frequency, shape='point')
	ia.modify(cl.torecord(), subtract=False)
	ia.done()
	cs.done()
	cl.done()

def create_ms(outname, modelname, corrupt=True, config='d'):
	'''
	This will simulate visibilities data based on the VLA

	Params
	------
	outname: str
		The filename for the created measurement set. The saved file will be `/sim/<outname>.int.ms'.
	modelname: str {'points'}
		The type of model to base the visiblities on. If not one of the above values, `ValueError` will
		be raised
	corrupt: bool (optional)
		If True, will corrupt the visiblities data based on simplenoise. Default=True
	config: str {'d'}
		The VLA configuration. If not one of the above options, `ValueError` will be raised
	'''
	# Clean the previous ms
	cleanfiles(outname, ms=True)
	try:
		sm.open('sim/' + outname + '.int.ms')
		u = simutil()
		configdir = os.getenv('CASAPATH').split()[0] + "/data/alma/simmos/"
		x, y, z, d, padnames, telescope, posobs = u.readantenna(configdir + 
				'vla.{}.cfg'.format(config))
		
		sm.setconfig(telescopename=telescope, x=x, y=y, z=z, dishdiameter=d.tolist(), 
			mount=['alt-az'], antname=padnames, coordsystem='global', 
			referencelocation=posobs)
		
		sm.setspwindow(spwname='CBand', freq=Frequency, deltafreq='10MHz', 
			freqresolution='10MHz', nchannels=100, stokes='RR')

		sm.setfield(sourcename='source', sourcedirection=center, 
			calcode='A') 
		sm.setlimits(shadowlimit=0.001, elevationlimit='8.0deg')
		sm.setauto(autocorrwt=0.0)
		sm.settimes(integrationtime='10s', usehourangle=False, referencetime=me.epoch('UTC', abstime))
		
		sm.observe('source', 'CBand', starttime='0s', stoptime='30000s')
		
		sm.setdata(spwid=1, fieldid=1)
		sm.setnoise(simplenoise=noise)
		
		if not modelname in list(models):
			raise ValueError('Not a saved model by that name')
		else:
			sm.predict(imagename=models[modelname])			

		if corrupt:
			sm.corrupt()
	finally:
		sm.close() 

def clean(name, **tclean_args):
	'''
	Cleans the given image based on assumed values

	Params
	------
	name: str
		The file to clean and output. Expects `sim/<name>.int.ms` and will save `sim/<name>.int.*`
		of various files.
	tclean_args: dict (optional)
		Any specific args to pass to tclean
	'''
	cleanfiles(name, inter=True)
	tclean(vis='sim/' + name + '.int.ms', imagename='sim/' + name + '.int', imsize=64, cell='7arcsec',
		niter=1000, threshold=noise, **tclean_args)

def sd_convolve(outname, modelname):
	'''
	Convolves the given model with a 2d kernel to simulate SD data

	Params
	------
	outname: str
		The name for file output. This will be saved at `sim/<outname>.sd.image`
	modelname: str {'points'}
		The model the image will be based on
	'''
	cleanfiles(outname, sd=True)
	if not modelname in list(models):
		raise ValueError('Not a saved model by that name')
	else:
		ia.open(models[modelname])
	axis = '{}rad'.format(3e8 / 5e9 * 1.22 / 100)
	im2 = ia.convolve2d(outfile='sim/' + outname + '.sd.image', type='gaussian', 
		major=axis, minor=axis, pa='0deg', overwrite=True)
	ia.close()
	im2.addnoise(type='uniform', pars=[0, 5e-6])
	im2.done()


def cleanfiles(name, ms=False, inter=False, sd=False):
	'''
	Cleans the data directory of files created by a simulation with the given path

	Params
	------
	path: str
		The pathname of the files
	
	ms: bool (optional)
		Cleans the measurment set data. Default=False
	inter: bool (optional)
		Cleans the cleaned image files. Default=False
	sd: bool (optional)
		Cleans the single dish image. Defalt=False
	'''
	if ms:
		print 'Removing the following files:'
		os.system('rm -rf sim/{}.int.ms'.format(name))
	if inter:
		print 'Removing the following files:'
		os.system('find sim -not -name {0}.int.ms -name {0}.int.* -print0 | xargs -0 rm -rf --'.format(name))
	if sd:
		print 'Removing the following files:'
		os.system('rm -rf sim/{}.sd.*'.format(name))


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('name', help='The filename prefix. \
			The files will be saved as <path>.sd.im and <path>.int.im')
	parser.add_argument('-m', '--model', default='points', help='The model for image creation.')
	parser.add_argument('-f', '--force', action='store_true', help='Will force create \
			the truth images for the simulations')
	parser.add_argument('-c', '--clean', action='store_true', help='Cleans the sim directory\
			of all files created by a given simulation name')
	args = parser.parse_args()

	if args.clean:
		n = os.system('find sim -name {}.* -print0'.format(args.name))
		confirm = raw_input('Are you sure you want to delete {} files? [y/(n)] '.format(n))
		if confirm.lower() == 'y':
			cleanfiles(args.name, True, True, True)
	else:
		simulate(args.name, args.model, args.force)
